﻿using System.Collections.ObjectModel;
using TabletPdfApp.Model;
using System.ComponentModel;
using System.Windows.Data;
using TabletPdfApp.View.Pages;
using TabletPdfApp.ViewModel.PagesViewModel;
using System.Windows.Controls;
using System;
using System.Windows;
using TabletPdfApp.Helper;
using System.IO;
using TabletPdfApp.Commands.TabManager;
using System.Linq;

namespace TabletPdfApp.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        private int numberOfTabs;

        public TabContent tabContent;

        private int selectedIndexTab;
        ICollectionView collectionView;
        public event PropertyChangedEventHandler PropertyChanged;

        public TabClose tabClose { get; set; }

        public int SelectedIndexTab
        {
            get { return selectedIndexTab; }
            set
            {
                if (selectedIndexTab != value)
                {
                    selectedIndexTab = value;
                    NotifyPropertyChanged("SelectedIndexTab");
                    //SelectedTabPropertyChanged();


                }
            }
        }
        private void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(info));
            }
        }
        private void SelectedTabPropertyChanged()
        {
            MessageBox.Show(selectedIndexTab.ToString());
            //TODO: when the tab is switched to another tab
        }

        public ObservableCollection<TabContent> ObservableCollectionTabContent { get; set; }
        public MainWindowViewModel()
        {
            GdPicture12.LicenseManager oLicenceManager = new GdPicture12.LicenseManager();
            oLicenceManager.RegisterKEY(Properties.Settings.Default.LicenseKey);

            //Donot unregister!! Need this always to create a new Tab.
            Mediator.Register("NewTabMainWindowViewModelMMVM", NewTabMediator);

            //Donot unregister!! Need this always to transfer Number of Tabs.
            Mediator.Register("OManagementSMainWindowViewModelVVM", OManagementSaveNumberofTabs);


            Mediator.Register("OManagementRMainWindowViewModelVVM", OManagementRToViewer);
            ObservableCollectionTabContent = new ObservableCollection<TabContent>();

            tabClose = new TabClose(this);
            collectionView = CollectionViewSource.GetDefaultView(this.ObservableCollectionTabContent);
        }

        private void NewTabMediator(object obj)
        {
            if (obj != null)
            {
                MediatorProperties mediatorProperties = (MediatorProperties)obj;
                Information1 info = (Information1)mediatorProperties.args;
                FileInfo fileInfo = new FileInfo(info.Title);
                NewTab(fileInfo.Name, mediatorProperties.View, mediatorProperties.CanClose);
            }


        }

        public void NewTab(String header, String navigatePageUri, bool canClose)
        {
            var tabItem = ObservableCollectionTabContent.Where(x => x.Header == header).SingleOrDefault();
            if (tabItem != null)
            {

                if (collectionView != null)
                {
                    collectionView.MoveCurrentTo(tabItem);
                }
            }
            else
            {
                Frame frame = new Frame();
                frame.Source = new Uri(navigatePageUri, UriKind.Relative);

                tabContent = new TabContent { Header = header, FrameSource = frame, CanClose = canClose };
                this.ObservableCollectionTabContent.Add(tabContent);
                numberOfTabs = ObservableCollectionTabContent.Count - 1;
                collectionView.MoveCurrentTo(tabContent);
            }

        }

        public void RemoveTab(object removeTab)
        {
            var tabItem = ObservableCollectionTabContent.Where(x => x.Header == removeTab as string).SingleOrDefault();
            //var tabItemIndex = ObservableCollectionTabContent.IndexOf(tabItem);
            //var item = ObservableCollectionTabContent. Contains();
            //.Items.Cast<TabItem>().Where(i => i.Name.Equals(tabName)).SingleOrDefault();
            this.ObservableCollectionTabContent.Remove(tabItem);
            Mediator.NotifyColleagues("OMEViewerViewModelMWVM", tabItem.Header);
            //MessageBox.Show(selectedIndexTab.ToString());
        }

        private void OManagementRToViewer(object obj)
        {
            var headerAsId = ObservableCollectionTabContent.ElementAt(SelectedIndexTab).Header;
            Mediator.NotifyColleagues("OMRViewerViewModelMWVM", headerAsId);
        }

        private void OManagementSaveNumberofTabs(object obj)
        {
            var headerAsId = ObservableCollectionTabContent.LastOrDefault().Header;
            Mediator.NotifyColleagues("OMSViewerViewModelMWVM", headerAsId);
        }
    }


}
