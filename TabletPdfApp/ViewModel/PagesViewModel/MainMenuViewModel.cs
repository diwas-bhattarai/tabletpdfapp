﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Linq;
using ReactiveUI;
using System.Windows;
using TabletPdfApp.Service;
using TabletPdfApp.Manager;
using TabletPdfApp.Helper;

namespace TabletPdfApp.ViewModel.PagesViewModel
{
    public class MainMenuViewModel<T> : ReactiveObject
    {

        private DataDownloadManager fileDownloadManager;
        private InformationService<T> dataService;
        private ObservableAsPropertyHelper<List<T>> searchResults;
        private string searchText;

        public T SelectedInformation { get; set; }

        public string SearchText
        {
            get { return searchText; }
            set { this.RaiseAndSetIfChanged(ref searchText, value); }
        }
        public List<T> SearchResults
        {
            get { return searchResults.Value; }
        }

        ObservableAsPropertyHelper<Visibility> spinnerVisibility;
        public Visibility SpinnerVisibility => spinnerVisibility.Value;
        public ReactiveCommand<List<T>> ExecuteSearch { get; protected set; }

        public MainMenuViewModel()
        {
            //Donot unregister!! Need this always to transfer SelectedInformation for each new Tab
            Mediator.Register("SelectedInformationMainMenuViewModelVVM", SelectedItemToView); 
            
            fileDownloadManager = new DataDownloadManager();
            dataService = new InformationService<T>(fileDownloadManager);
            ExecuteSearch = ReactiveCommand.CreateAsyncTask(parameter => dataService.ReturnSearch(SearchText));
            this.WhenAnyValue(x => x.SearchText)
                   .Throttle(TimeSpan.FromMilliseconds(800), RxApp.MainThreadScheduler)
                   .DistinctUntilChanged()
                   .Where(x => !String.IsNullOrWhiteSpace(x))
                   .InvokeCommand(ExecuteSearch);

            spinnerVisibility = ExecuteSearch.IsExecuting
         .Select(x => x ? Visibility.Visible : Visibility.Collapsed)
         .ToProperty(this, x => x.SpinnerVisibility, Visibility.Hidden);

            ExecuteSearch.ThrownExceptions.Subscribe(ex =>
            {
                MessageBox.Show("Error!!!");

            });

            searchResults = ExecuteSearch.ToProperty(this, x => x.SearchResults, new List<T>());

        }


        public void doubleclicknewtab()
        {
            if (SelectedInformation != null)
            {
                MediatorProperties mediatorProperties = new MediatorProperties();
                mediatorProperties.Header = "SelectedInformation";
                mediatorProperties.View = "View/Pages/Viewer.xaml";
                mediatorProperties.CanClose = true;
                mediatorProperties.args = SelectedInformation;
                Mediator.NotifyColleagues("NewTabMainWindowViewModelMMVM", mediatorProperties);
            }

            
        }

        private void SelectedItemToView(object obj)
        {
            if (obj != null)
            {
                Mediator.NotifyColleagues("OpenPdfViewerViewModelMMVM", SelectedInformation);
            }
        }
    }

}
