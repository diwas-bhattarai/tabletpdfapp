﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using TabletPdfApp.Commands;

namespace TabletPdfApp.ViewModel.PagesViewModel
{
    public class EditorViewModel
    {
        public EditorCommands EditorCommands;
        public ViewerCommand ViewerCommand;

        public EditorViewModel()
        {

        }


        public EditorViewModel(Page page, GdPicture12.WPF.GdViewer GdViewer)
        {
            EditorCommands = new EditorCommands(GdViewer);
            ViewerCommand = new ViewerCommand();
        }
    }
}
