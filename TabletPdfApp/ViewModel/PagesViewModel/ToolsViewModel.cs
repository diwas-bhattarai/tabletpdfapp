﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace TabletPdfApp.ViewModel.PagesViewModel
{
    public  class ToolsViewModel
    {
            public List<BitmapImage> LoadImages()
            {
                List<BitmapImage> robotImages = new List<BitmapImage>();
                DirectoryInfo robotImageDir = new DirectoryInfo(@"..\..\Robots");
                foreach (FileInfo robotImageFile in robotImageDir.GetFiles("*.jpg"))
                {
                    Uri uri = new Uri(robotImageFile.FullName);
                    robotImages.Add(new BitmapImage(uri));
                }
                return robotImages;
            }
        
    }
}
