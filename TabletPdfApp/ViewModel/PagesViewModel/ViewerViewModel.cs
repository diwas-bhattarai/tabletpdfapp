﻿using System;
using TabletPdfApp.Commands;
using System.Windows.Controls;
using TabletPdfApp.Commands.CustomCommands;
using System.Windows.Input;
using ReactiveUI;
using TabletPdfApp.Manager;
using TabletPdfApp.Service;
using System.IO;
using TabletPdfApp.Model;
using System.Windows;
using TabletPdfApp.Helper;
using GdPicture12;

namespace TabletPdfApp.ViewModel.PagesViewModel
{
    public class ViewerViewModel : ReactiveObject
    {

        private DataDownloadManager fileDownloadManager;
        private ViewerService viewerService;
        private GdPicture12.WPF.GdViewer PdfViewer;
        private GdPicturePDF oGdpicture;
        public ViewerCommand ViewerCommand;

        public ViewerViewModel(Page page, GdPicture12.WPF.GdViewer GdViewer)
        {
            this.PdfViewer = GdViewer;
            ViewerCommand = new ViewerCommand();
            new ViewerCustomCommand(page, ViewerCommand);
            fileDownloadManager = new DataDownloadManager();
            viewerService = new ViewerService(fileDownloadManager);
            Init();
        }

        public void Init()
        {
            Mediator.Register("OpenPdfViewerViewModelMMVM", OpenNewPdf);
            Mediator.Register("OMSViewerViewModelMWVM", SaveObject);
            Mediator.Register("OMEViewerViewModelMWVM", EditSavedObjects);
            Mediator.NotifyColleagues("SelectedInformationMainMenuViewModelVVM", "SelectedInformationMainMenuViewModelVVM");

            Mediator.Unregister("OpenPdfViewerViewModelMMVM", OpenNewPdf);
            Mediator.Unregister("OMSViewerViewModelMWVM", SaveObject);
        }

       

        public void OpenNewPdf(object obj)
        {
            if (obj != null)
            {
                this.oGdpicture = ObjectManagement.GetInstance().GetnewObject();
                Information1 info = (Information1)obj;
                var tempStream = new MemoryStream();
                var result = viewerService.DownloadFile(info.Title);
                result.CopyTo(tempStream);
                this.oGdpicture.LoadFromStream(tempStream);
                Mediator.NotifyColleagues("OManagementSMainWindowViewModelVVM", "numberOfTabs");
            }
        }

        private void SaveObject(object obj)
        {
            ObjectManagement.GetInstance().Save(obj, PdfViewer, this.oGdpicture);
        }

        private void RetrieveObject(object obj)
        {
            var tupleGdPicture = ObjectManagement.GetInstance().Retrieve(obj as String);
            tupleGdPicture.Item1.CloseDocument();
            try
            {
                tupleGdPicture.Item1.DisplayFromGdPicturePDF(tupleGdPicture.Item2);
                ViewerCommand.GdpictureObject(tupleGdPicture.Item1, tupleGdPicture.Item2);

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public void EditSavedObjects(object obj)
        {
            ObjectManagement.GetInstance().Dispose(obj as String);
        }


        public void ViewSelectedFile()
        {
            Mediator.Register("OMRViewerViewModelMWVM", RetrieveObject);
            Mediator.NotifyColleagues("OManagementRMainWindowViewModelVVM", "SelectedIndexTab");
            Mediator.Unregister("OMRViewerViewModelMWVM", RetrieveObject);
        }


        public void Dispose()
        {
          
        }

    }

    public class ViewerCustomCommand
    {
      
        
        public ViewerCustomCommand(Page page, ViewerCommand viewerCommand)
        {
            Initiate(page, viewerCommand);
        }

        public void Initiate(Page page, ViewerCommand viewerCommand)
        {
            page.CommandBindings.Add(
            new CommandBinding(CustomCommands.Exit, viewerCommand.Exit_Executed, viewerCommand.Exit_CanExecute));
            page.CommandBindings.Add(
            new CommandBinding(CustomCommands.Pen, viewerCommand.Pen_Executed, viewerCommand.Pen_CanExecute));
        }

    }

}
