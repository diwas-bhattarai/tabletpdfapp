﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using TabletPdfApp.Model;
using TabletPdfApp.ViewModel;

namespace TabletPdfApp.Commands.TabManager
{
    public class TabClose : ICommand
    {

        public MainWindowViewModel mainWindowViewModel { get; set; }
      
        public TabClose(MainWindowViewModel mainWindowViewModel)
        {
            this.mainWindowViewModel = mainWindowViewModel;
        }
      

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            //var tabItem = ObservableCollectionTabContent.Where(x => x.Header == parameter as string);
            //MessageBox.Show(s);
            this.mainWindowViewModel.RemoveTab(parameter);
        }
    }
}
