﻿using GdPicture12;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace TabletPdfApp.Commands
{
    public class ViewerCommand
    {
        GdPicture12.WPF.GdViewer PdfViewer;
        GdPicturePDF GdPicturePDF;
        public ViewerCommand()
        {
           
        }

        public void GdpictureObject(GdPicture12.WPF.GdViewer pdfViewer, GdPicturePDF gdPicturePDF)
        {
            this.PdfViewer = pdfViewer;
            this.GdPicturePDF = gdPicturePDF;

        }


        public void Exit_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PdfViewer.AddFreeHandHighlighterAnnotInteractive(Colors.Yellow, 0.020f);
        }

        public void Exit_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        public void Pen_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            PdfViewer.SaveAnnotationsToPage();
        }

        public void Pen_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }


}
