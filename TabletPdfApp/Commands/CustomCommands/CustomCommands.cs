﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TabletPdfApp.Commands.CustomCommands
{
    public static class CustomCommands
    {
        public static readonly RoutedUICommand Exit = new RoutedUICommand("Exit", "Exit", typeof(CustomCommands), null);
        public static readonly RoutedUICommand Pen = new RoutedUICommand("Pen", "Pen", typeof(CustomCommands), null);

    }
}
