﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TabletPdfApp.Manager;

namespace TabletPdfApp.Service
{
    public class InformationService<T>
    {

        DataDownloadManager fileDownloadManager;
        public InformationService(DataDownloadManager fileDownloadManager)
        {
            this.fileDownloadManager = fileDownloadManager;
        }

        public async Task<List<T>> ReturnSearch(String searchText)
        {
            string[] searchTextParameter = { searchText };
            var results = await Task.Run(() => fileDownloadManager.GetResponseFromServer(
                Properties.Settings.Default.RestUri, Properties.Settings.Default.InfoRestApi, searchTextParameter));
            var serializedData = ServiceHelper.JsonSerializer<T>(results);
            return serializedData;
        }

    }
}