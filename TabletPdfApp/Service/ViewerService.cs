﻿using GdPicture12;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TabletPdfApp.Helper;
using TabletPdfApp.Manager;

namespace TabletPdfApp.Service
{
    public class ViewerService
    {
        private DataDownloadManager fileDownloadManager;
        
        public ViewerService(DataDownloadManager fileDownloadManager)
        {
            this.fileDownloadManager = fileDownloadManager;
        }
        public  Stream DownloadFile(String searchText)
        {
            string[] searchTextParameter = { searchText };
            var result = fileDownloadManager.StreamFromServer(
                Properties.Settings.Default.RestUri, Properties.Settings.Default.DownloadRestApi, searchTextParameter);
            return result;
        }

    }
}
