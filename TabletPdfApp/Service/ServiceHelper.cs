﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace TabletPdfApp.Service
{
    public static class ServiceHelper
    {
        public static List<T> JsonSerializer<T>(String searchResults)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<T>));
            MemoryStream ms = new MemoryStream(System.Text.ASCIIEncoding.ASCII.GetBytes(searchResults));
            List<T> deSeSearchResults = (List<T>)serializer.ReadObject(ms);
            ms.Close();
            return deSeSearchResults;
        }
    }
}
