﻿using System.Windows;
using TabletPdfApp.ViewModel;
using MahApps.Metro.Controls;
using TabletPdfApp.Helper;
using System;
using MahApps.Metro.Controls.Dialogs;
using TabletPdfApp.Cultures;
using System.Globalization;
using System.Windows.Controls;

namespace TabletPdfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {


        private MainWindowViewModel mainWindowViewModel;
        private MetroWindow metroWindow;
        public MainWindow()
        {
            CultureResources.GetInstance().ChangeCulture(new CultureInfo("en"));
            InitializeComponent();
            mainWindowViewModel = new MainWindowViewModel();
            this.DataContext = mainWindowViewModel;
            Mediator.Register("MessageBox", ShowMessageBox);
            metroWindow = Application.Current.MainWindow as MetroWindow;
        }

        private async void ShowMessageBox(object obj)
        {
            if (obj != null)
            {
                try
                {
                    metroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented; // set the theme
                    await metroWindow.ShowMessageAsync(obj.ToString(), obj.ToString(), MessageDialogStyle.Affirmative);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mainWindowViewModel.NewTab(Cultures.Resources.Home, "View/Pages/MainMenu.xaml", false);
            mainWindowViewModel.NewTab("Tools", "View/Pages/Tools.xaml", false);
            //mainWindowViewModel.NewTab("PdfViewer", "View/Pages/Viewer.xaml");
        }

        private void tabCloseButton_Click(object sender, RoutedEventArgs e)
        {
            string tabName = (sender as Button).CommandParameter.ToString();

            mainWindowViewModel.RemoveTab(tabName);
        }
    }
}
