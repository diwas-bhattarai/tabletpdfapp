﻿using System.Windows;
using System.Windows.Controls;
using GdPicture12;
using TabletPdfApp.ViewModel.PagesViewModel;
using System.Threading.Tasks;
using System.IO;
using TabletPdfApp.Helper;

namespace TabletPdfApp.View.Pages
{
    /// <summary>
    /// Interaction logic for PdfViewer.xaml
    /// </summary>
    public partial class Viewer : Page
    {
        ViewerViewModel viewerViewModel;
        public Viewer()
        {
          
            InitializeComponent();
            viewerViewModel = new ViewerViewModel(this, PdfViewer);
            this.DataContext = viewerViewModel;
           
            //BookmarksTreeTab.LoadFromGdViewer(PdfViewer);
            //ThumbnailTab.LoadFromGdViewer(PdfViewer);
        }


        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            viewerViewModel.ViewSelectedFile();
            //ThumbnailTab.LoadFromGdViewer(PdfViewer);
            //BookmarksTreeTab.LoadFromGdViewer(PdfViewer);
        }

        private void Page_Unloaded(object sender, RoutedEventArgs e)
        {
         
        }
    }
}
