﻿using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using TabletPdfApp.ViewModel.PagesViewModel;
using TabletPdfApp.Model;
using TabletPdfApp.ViewModel;
using TabletPdfApp.Helper;
using System.Globalization;
using TabletPdfApp.Cultures;

namespace TabletPdfApp.View.Pages
{
    /// <summary>
    /// Interaction logic for MainMenu.xaml
    /// </summary>
    public partial class MainMenu : Page
    {
        //MetroWindow metroWindow = Application.Current.MainWindow as MetroWindow;
        MainMenuViewModel<Information1> mainMenuViewModel;
        public   MainMenu()
        {
        
           

            mainMenuViewModel = new MainMenuViewModel<Information1>();
            this.DataContext = mainMenuViewModel;
            InitializeComponent();
          
        }

        private void DataGrid_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            mainMenuViewModel.doubleclicknewtab();
        }


        //public async void MessageBox()
        //{
        //    metroWindow.MetroDialogOptions.ColorScheme = MetroDialogColorScheme.Accented; // set the theme

        //    await metroWindow.ShowMessageAsync("This is the title", "Some message", MessageDialogStyle.Affirmative);
        //}


    }
}
