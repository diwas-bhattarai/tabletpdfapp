﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TabletPdfApp.ViewModel.PagesViewModel;

namespace TabletPdfApp.View.Pages
{
    /// <summary>
    /// Interaction logic for Tools.xaml
    /// </summary>
    public partial class Tools : Page
    {
        ToolsViewModel toolsViewModel;
        public Tools()
        {
            InitializeComponent();
            toolsViewModel = new ToolsViewModel();
            asdf.ItemsSource = toolsViewModel.LoadImages();

        }
     
    }
}
