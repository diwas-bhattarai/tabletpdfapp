﻿using GdPicture12;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TabletPdfApp.Helper
{
    public class ObjectManagement
    {
        Dictionary<String, Tuple<GdPicture12.WPF.GdViewer, GdPicturePDF>> dictionary;

        private static ObjectManagement instance;

        public static ObjectManagement GetInstance()
        {
            if (instance == null)
            {
                instance = new ObjectManagement();
            }
            return instance;
        }


        public ObjectManagement()
        {
            dictionary = new Dictionary<String, Tuple<GdPicture12.WPF.GdViewer, GdPicturePDF>>();
        }

        public GdPicturePDF GetnewObject()
        {
            return new GdPicturePDF();
        }

        public void Save(object tabIndex, GdPicture12.WPF.GdViewer pdfViewer, GdPicturePDF oGdPicturePDF)
        {
            try
            {
                dictionary.Add(tabIndex as string, new Tuple<GdPicture12.WPF.GdViewer, GdPicturePDF>(pdfViewer, oGdPicturePDF));
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public Tuple<GdPicture12.WPF.GdViewer, GdPicturePDF> Retrieve(String tabIndex)
        {
            Tuple<GdPicture12.WPF.GdViewer, GdPicturePDF> value = null;
            if (dictionary.ContainsKey(tabIndex))
            {
                value = dictionary[tabIndex];
            }
            return value;
        }


        public void Dispose(String KeyToDispose)
        {

            if (KeyToDispose == "")
            {
                foreach (var entry in dictionary)
                {
                    entry.Value.Item1.Dispose();
                    entry.Value.Item2.Dispose();
                }
            }
            else
            {
                var toDispose = Retrieve(KeyToDispose);
                toDispose.Item1.Dispose();
                toDispose.Item2.Dispose();
                dictionary.Remove(KeyToDispose);
            }





        }


    }


}
