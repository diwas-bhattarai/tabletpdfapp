﻿using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using TabletPdfApp.Model;
using System.Runtime.Serialization.Json;
using System.Collections.Generic;
using System.Windows;

namespace TabletPdfApp.Manager
{
    public class DataDownloadManager
    {
        public DataDownloadManager()
        {

        }

        public String GetResponseFromServer(String serviceEndPoint, String serviceApi, String[] parameters)
        {
            String data = null;
            String apiFinalUri = GetServiceEndPoint(serviceEndPoint, serviceApi, parameters);
            HttpWebRequest request = CreateHttpRequest(apiFinalUri);
            using (HttpWebResponse response =
                (HttpWebResponse)request.GetResponse())
            {

                data = ReadResponse(response);
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    //LogMsg(string.Format("Error: {0}", data));
                    //LogMsg(string.Format(
                    //    "Unexpected status code returned: {0}",
                    //    response.StatusCode));
                }

            }
            return data;
        }

     

        public Stream StreamFromServer(String serviceEndPoint, String serviceApi, String[] parameters)
        {
            String apiFinalUri = GetServiceEndPoint(serviceEndPoint, serviceApi, parameters);
            HttpWebRequest request = CreateHttpRequest(apiFinalUri);
            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)request.GetResponse();

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());

            }

            return response.GetResponseStream();
        }

        private HttpWebRequest CreateHttpRequest(String apiFinalUri)
        {

            HttpWebRequest request =
                (HttpWebRequest)WebRequest.Create(apiFinalUri);
            request.Method = "GET";
            request.Accept = "application/json";
            //request.Headers.Add(HttpRequestHeader.Authorization, oAuthToken);
            return request;
        }

        private static String GetServiceEndPoint(String serviceEndPoint, String serviceApi, String[] parameters)
        {
            String apiFinalUri = null;
            if (serviceApi != "DownloadFile")
            {

                String combinedParameters = "";
                for (int i = 0; i < parameters.Length; i++)
                {
                    combinedParameters = combinedParameters + "/" + parameters[i];
                }
                apiFinalUri = serviceEndPoint + "/" + serviceApi + combinedParameters;
            }
            else
            {
                //String combinedParameters = "";
                //for (int i = 0; i < parameters.Length; i++)
                //{
                //    combinedParameters = combinedParameters + "?" + parameters[i];
                //}
                apiFinalUri = serviceEndPoint + "/" + serviceApi + "?fileToDownload="+ parameters[0];
            }
            return apiFinalUri;
        }

        private String ReadResponse(HttpWebResponse response)
        {
            // Begin by validating our inbound parameters.
            if (response == null)
            {
                throw new ArgumentNullException("response",
                    "Value cannot be null");
            }

            // Then, open up a reader to the response and read the contents 
            // to a string and return that to the caller.
            String responseBody = "";
            using (Stream rspStm = response.GetResponseStream())
            {
                using (StreamReader reader = new StreamReader(rspStm))
                {
                    responseBody = reader.ReadToEnd();
                }
            }
            return responseBody;
        }

    }
}
