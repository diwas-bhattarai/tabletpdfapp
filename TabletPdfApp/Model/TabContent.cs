﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace TabletPdfApp.Model
{
    public sealed class TabContent
    {
            public String Header { get; set; }
            public FrameworkElement FrameSource { get; set; }
            public bool CanClose { get; set; }
    }
}
