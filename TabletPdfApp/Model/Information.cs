﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TabletPdfApp.Model
{
    public class Information1
    {
        public String Title { get; set; }
        public long Size { get; set; }
        public DateTime Date { get; set; }
    }
}
